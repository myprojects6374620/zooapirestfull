package com.example.zooapirestful.dao;

import com.example.zooapirestful.model.Nutrition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NutritionDAO extends JpaRepository<Nutrition, Long> {
}
