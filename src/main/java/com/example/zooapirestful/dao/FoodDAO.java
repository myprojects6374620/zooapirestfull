package com.example.zooapirestful.dao;

import com.example.zooapirestful.model.Food;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FoodDAO extends JpaRepository<Food, Long> {
}
