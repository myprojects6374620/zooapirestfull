package com.example.zooapirestful.dao;

import com.example.zooapirestful.model.FeedingPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedingPlanDAO extends JpaRepository<FeedingPlan, Long> {
}
