package com.example.zooapirestful.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.zooapirestful.model.Alert;

public interface AlertDAO extends JpaRepository<Alert, Long> {
}
