package com.example.zooapirestful.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.zooapirestful.model.Animal;

public interface AnimalDAO extends JpaRepository<Animal, Long> {
}
