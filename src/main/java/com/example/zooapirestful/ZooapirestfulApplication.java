package com.example.zooapirestful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = "com.example.zooapirestful.model")
@EnableJpaRepositories(basePackages = "com.example.zooapirestful.dao")
public class ZooapirestfulApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZooapirestfulApplication.class, args);
	}
}
