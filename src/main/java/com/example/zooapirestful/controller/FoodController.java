package com.example.zooapirestful.controller;

import com.example.zooapirestful.model.Food;
import com.example.zooapirestful.service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/foods")
public class FoodController {

    @Autowired
    private FoodService foodService;

    @PostMapping
    public ResponseEntity<EntityModel<Food>> createFood(@RequestBody Food food) {
        Food savedFood = foodService.save(food);
        EntityModel<Food> resource = EntityModel.of(savedFood);
        resource.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).findById(savedFood.getId())).withSelfRel());
        return ResponseEntity.ok(resource);
    }

    @GetMapping
    public ResponseEntity<Page<EntityModel<Food>>> findAll(Pageable pageable) {
        Page<Food> foods = foodService.findAll(pageable);
        Page<EntityModel<Food>> resources = foods.map(food -> EntityModel.of(food).add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).findById(food.getId())).withSelfRel()));
        return ResponseEntity.ok(resources);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EntityModel<Food>> findById(@PathVariable Long id) {
        Optional<Food> food = foodService.findById(id);
        if (food.isPresent()) {
            EntityModel<Food> resource = EntityModel.of(food.get());
            resource.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).findById(id)).withSelfRel());
            return ResponseEntity.ok(resource);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) {
        if (foodService.existsById(id)) {
            foodService.deleteById(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
