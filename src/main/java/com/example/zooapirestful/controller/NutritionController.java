package com.example.zooapirestful.controller;

import com.example.zooapirestful.model.Nutrition;
import com.example.zooapirestful.service.NutritionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/nutritions")
public class NutritionController {

    @Autowired
    private NutritionService nutritionService;

    @PostMapping
    public ResponseEntity<EntityModel<Nutrition>> createNutrition(@RequestBody Nutrition nutrition) {
        Nutrition savedNutrition = nutritionService.save(nutrition);
        EntityModel<Nutrition> resource = EntityModel.of(savedNutrition);
        resource.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).findById(savedNutrition.getId())).withSelfRel());
        return ResponseEntity.ok(resource);
    }

    @GetMapping
    public ResponseEntity<Page<EntityModel<Nutrition>>> findAll(Pageable pageable) {
        Page<Nutrition> nutritions = nutritionService.findAll(pageable);
        Page<EntityModel<Nutrition>> resources = nutritions.map(nutrition -> EntityModel.of(nutrition).add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).findById(nutrition.getId())).withSelfRel()));
        return ResponseEntity.ok(resources);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EntityModel<Nutrition>> findById(@PathVariable Long id) {
        Optional<Nutrition> nutrition = nutritionService.findById(id);
        if (nutrition.isPresent()) {
            EntityModel<Nutrition> resource = EntityModel.of(nutrition.get());
            resource.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).findById(id)).withSelfRel());
            return ResponseEntity.ok(resource);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) {
        if (nutritionService.existsById(id)) {
            nutritionService.deleteById(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
