package com.example.zooapirestful.controller;

import com.example.zooapirestful.model.Animal;
import com.example.zooapirestful.service.AnimalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/animals")
public class AnimalController {

    @Autowired
    private AnimalService animalService;

    @PostMapping
    public ResponseEntity<EntityModel<Animal>> createAnimal(@RequestBody Animal animal) {
        Animal createdAnimal = animalService.save(animal);
        EntityModel<Animal> entityModel = EntityModel.of(createdAnimal,
                WebMvcLinkBuilder.linkTo(methodOn(AnimalController.class).getAnimalById(createdAnimal.getId())).withSelfRel(),
                WebMvcLinkBuilder.linkTo(methodOn(AnimalController.class).getAllAnimals(Pageable.unpaged())).withRel("animals"));

        return ResponseEntity
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(entityModel);
    }

    @GetMapping
    @Cacheable("animals")
    public ResponseEntity<CollectionModel<EntityModel<Animal>>> getAllAnimals(Pageable pageable) {
        Page<Animal> animals = animalService.findAll(pageable);
        CollectionModel<EntityModel<Animal>> collectionModel = CollectionModel.of(
                animals.stream()
                        .map(animal -> EntityModel.of(animal,
                                WebMvcLinkBuilder.linkTo(methodOn(AnimalController.class).getAnimalById(animal.getId())).withSelfRel()))
                        .toList());

        Link link = WebMvcLinkBuilder.linkTo(methodOn(AnimalController.class).getAllAnimals(pageable)).withSelfRel();
        collectionModel.add(link);

        return ResponseEntity.ok(collectionModel);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EntityModel<Animal>> getAnimalById(@PathVariable Long id) {
        Optional<Animal> animalOptional = animalService.findById(id);

        if (animalOptional.isPresent()) {
            Animal animal = animalOptional.get();
            EntityModel<Animal> entityModel = EntityModel.of(animal,
                    WebMvcLinkBuilder.linkTo(methodOn(AnimalController.class).getAnimalById(id)).withSelfRel(),
                    WebMvcLinkBuilder.linkTo(methodOn(AnimalController.class).getAllAnimals(Pageable.unpaged())).withRel("animals"));

            return ResponseEntity.ok(entityModel);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<EntityModel<Animal>> updateAnimal(@PathVariable Long id, @RequestBody Animal animalDetails) {
        Optional<Animal> animalOptional = animalService.findById(id);

        if (animalOptional.isPresent()) {
            Animal animal = animalOptional.get();
            animal.setName(animalDetails.getName());
            animal.setSpecies(animalDetails.getSpecies());
            animal.setAge(animalDetails.getAge());
            animal.setDiet(animalDetails.getDiet());
            Animal updatedAnimal = animalService.save(animal);

            EntityModel<Animal> entityModel = EntityModel.of(updatedAnimal,
                    WebMvcLinkBuilder.linkTo(methodOn(AnimalController.class).getAnimalById(updatedAnimal.getId())).withSelfRel(),
                    WebMvcLinkBuilder.linkTo(methodOn(AnimalController.class).getAllAnimals(Pageable.unpaged())).withRel("animals"));

            return ResponseEntity.ok(entityModel);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAnimal(@PathVariable Long id) {
        if (animalService.existsById(id)) {
            animalService.deleteById(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
