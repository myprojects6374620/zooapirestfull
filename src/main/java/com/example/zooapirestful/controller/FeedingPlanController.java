package com.example.zooapirestful.controller;

import com.example.zooapirestful.model.FeedingPlan;
import com.example.zooapirestful.service.FeedingPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/feedingPlans")
public class FeedingPlanController {

    @Autowired
    private FeedingPlanService feedingPlanService;

    @PostMapping
    public ResponseEntity<EntityModel<FeedingPlan>> createFeedingPlan(@RequestBody FeedingPlan feedingPlan) {
        FeedingPlan savedFeedingPlan = feedingPlanService.save(feedingPlan);
        EntityModel<FeedingPlan> resource = EntityModel.of(savedFeedingPlan);
        resource.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).findById(savedFeedingPlan.getId())).withSelfRel());
        return ResponseEntity.ok(resource);
    }

    @GetMapping
    public ResponseEntity<Page<EntityModel<FeedingPlan>>> findAll(Pageable pageable) {
        Page<FeedingPlan> feedingPlans = feedingPlanService.findAll(pageable);
        Page<EntityModel<FeedingPlan>> resources = feedingPlans.map(feedingPlan -> EntityModel.of(feedingPlan).add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).findById(feedingPlan.getId())).withSelfRel()));
        return ResponseEntity.ok(resources);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EntityModel<FeedingPlan>> findById(@PathVariable Long id) {
        Optional<FeedingPlan> feedingPlan = feedingPlanService.findById(id);
        if (feedingPlan.isPresent()) {
            EntityModel<FeedingPlan> resource = EntityModel.of(feedingPlan.get());
            resource.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).findById(id)).withSelfRel());
            return ResponseEntity.ok(resource);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) {
        if (feedingPlanService.existsById(id)) {
            feedingPlanService.deleteById(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
