package com.example.zooapirestful.service;

import com.example.zooapirestful.dao.NutritionDAO;
import com.example.zooapirestful.model.Nutrition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class NutritionService {

    @Autowired
    private NutritionDAO nutritionDAO;

    @CacheEvict(value = "nutritions", allEntries = true)
    public Nutrition save(Nutrition nutrition) {
        return nutritionDAO.save(nutrition);
    }

    @Cacheable("nutritions")
    public Page<Nutrition> findAll(Pageable pageable) {
        return nutritionDAO.findAll(pageable);
    }

    public Optional<Nutrition> findById(Long id) {
        return nutritionDAO.findById(id);
    }

    @CacheEvict(value = "nutritions", allEntries = true)
    public void deleteById(Long id) {
        nutritionDAO.deleteById(id);
    }

    public boolean existsById(Long id) {
        return nutritionDAO.existsById(id);
    }
}
