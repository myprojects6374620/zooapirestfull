package com.example.zooapirestful.service;

import com.example.zooapirestful.dao.FeedingPlanDAO;
import com.example.zooapirestful.model.FeedingPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FeedingPlanService {

    @Autowired
    private FeedingPlanDAO feedingPlanDAO;

    @CacheEvict(value = "feedingPlans", allEntries = true)
    public FeedingPlan save(FeedingPlan feedingPlan) {
        return feedingPlanDAO.save(feedingPlan);
    }

    @Cacheable("feedingPlans")
    public Page<FeedingPlan> findAll(Pageable pageable) {
        return feedingPlanDAO.findAll(pageable);
    }

    public Optional<FeedingPlan> findById(Long id) {
        return feedingPlanDAO.findById(id);
    }

    @CacheEvict(value = "feedingPlans", allEntries = true)
    public void deleteById(Long id) {
        feedingPlanDAO.deleteById(id);
    }

    public boolean existsById(Long id) {
        return feedingPlanDAO.existsById(id);
    }
}
