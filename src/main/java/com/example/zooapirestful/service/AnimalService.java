package com.example.zooapirestful.service;

import com.example.zooapirestful.dao.AnimalDAO;
import com.example.zooapirestful.model.Animal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AnimalService {

    @Autowired
    private AnimalDAO animalDAO;

    @CacheEvict(value = "animals", allEntries = true)
    public Animal save(Animal animal) {
        return animalDAO.save(animal);
    }

    @Cacheable("animals")
    public Page<Animal> findAll(Pageable pageable) {
        return animalDAO.findAll(pageable);
    }

    public Optional<Animal> findById(Long id) {
        return animalDAO.findById(id);
    }

    @CacheEvict(value = "animals", allEntries = true)
    public void deleteById(Long id) {
        animalDAO.deleteById(id);
    }

    public boolean existsById(Long id) {
        return animalDAO.existsById(id);
    }
}
