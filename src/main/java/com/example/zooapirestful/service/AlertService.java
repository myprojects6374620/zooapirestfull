package com.example.zooapirestful.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.zooapirestful.dao.AlertDAO;
import com.example.zooapirestful.model.Alert;
import java.util.List;
import java.util.Optional;

@Service
public class AlertService {

    @Autowired
    private AlertDAO alertDAO;

    public List<Alert> getAllAlerts() {
        return alertDAO.findAll();
    }

    public Optional<Alert> getAlertById(Long id) {
        return alertDAO.findById(id);
    }

    public Alert createAlert(Alert alert) {
        return alertDAO.save(alert);
    }

    public void deleteAlert(Long id) {
        alertDAO.deleteById(id);
    }
}

