package com.example.zooapirestful.service;

import com.example.zooapirestful.dao.FoodDAO;
import com.example.zooapirestful.model.Food;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FoodService {

    @Autowired
    private FoodDAO foodDAO;

    @CacheEvict(value = "foods", allEntries = true)
    public Food save(Food food) {
        return foodDAO.save(food);
    }

    @Cacheable("foods")
    public Page<Food> findAll(Pageable pageable) {
        return foodDAO.findAll(pageable);
    }

    public Optional<Food> findById(Long id) {
        return foodDAO.findById(id);
    }

    @CacheEvict(value = "foods", allEntries = true)
    public void deleteById(Long id) {
        foodDAO.deleteById(id);
    }

    public boolean existsById(Long id) {
        return foodDAO.existsById(id);
    }
}
