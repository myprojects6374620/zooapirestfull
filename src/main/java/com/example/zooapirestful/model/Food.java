package com.example.zooapirestful.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
public class Food {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String foodName;
    private String foodType;

    @ManyToOne
    @JoinColumn(name = "nutrition_id")
    private Nutrition nutrition;
}
