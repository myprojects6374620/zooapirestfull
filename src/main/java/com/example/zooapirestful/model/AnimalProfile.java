package com.example.zooapirestful.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Service;

@Service
@Component
public class AnimalProfile {
    private final ModelMapper modelMapper;
    @Autowired
    public AnimalProfile(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
        modelMapper.addMappings(new PropertyMap<Animal, AnimalDTO>() {
            @Override
            protected void configure() {
                map().setId(source.getId());
                map().setName(source.getName());
                map().setSpecies(source.getSpecies());
                map().setAge(source.getAge());
                map().setDiet(source.getDiet());
            }
        });
    }
}

