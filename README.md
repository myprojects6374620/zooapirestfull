# Zoo API RESTful

This project is a RESTful API for managing and alerting the nutrition of zoo animals. 
The API allows managing animals, their feeding records, and related alerts.

## Richardson Maturity Model

The API design adheres to the Richardson Maturity Model (RMM) and achieves Level 3 
through the implementation of HATEOAS (Hypermedia as the Engine of Application State). 
This enables clients to navigate through the API dynamically, discovering available
 actions based on the current application state.

## Project Structure

![Project Structure](images/structure.png)

## Entities

### Animal
- **ID:** Unique identifier of the animal.
- **Name:** Name of the animal.
- **Species:** Species of the animal.
- **Age:** Age of the animal.
- **Diet:** Description of the animal's diet.

### Feeding
- **ID:** Unique identifier of the feeding record.
- **AnimalID:** Identifier of the animal.
- **Date:** Date of the feeding record.
- **Food:** Type of food provided.
- **Quantity:** Amount of food provided.

### Alert
- **ID:** Unique identifier of the alert.
- **AnimalID:** Identifier of the animal.
- **Type:** Type of alert (e.g., "Feeding", "Health").
- **Date:** Date of the alert.
- **Message:** Descriptive message of the alert.

### Food
- **ID:** Unique identifier of the food.
- **Name:** Name of the food.
- **Type:** Type of food (e.g., Vegetable, Meat).
- **Nutrients:** Description of the food's nutrients.

### Nutrition
- **ID:** Unique identifier of the nutrition.
- **AnimalID:** Identifier of the animal.
- **Diet:** Description of the animal's specific diet.
- **Nutrients:** Description of the necessary nutrients.

### FeedingPlan
- **ID:** Unique identifier of the feeding plan.
- **AnimalID:** Identifier of the animal.
- **Name:** Name of the feeding plan.
- **Description:** Detailed description of the feeding plan.

## API Operations

### Animal

- **Create Animal**
  - **Method:** POST
  - **Path:** `/animals`
  - **Status Code:** 201 Created

- **Get Animals**
  - **Method:** GET
  - **Path:** `/animals`
  - **Filters:** `species`, `age`
  - **Pagination:** `page`, `size`
  - **Status Code:** 200 OK

- **Get Animal by ID**
  - **Method:** GET
  - **Path:** `/animals/{id}`
  - **Status Code:** 200 OK, 404 Not Found

- **Update Animal**
  - **Method:** PUT
  - **Path:** `/animals/{id}`
  - **Status Code:** 200 OK, 404 Not Found

- **Delete Animal**
  - **Method:** DELETE
  - **Path:** `/animals/{id}`
  - **Status Code:** 204 No Content, 404 Not Found

### Feeding

- **Create Feeding**
  - **Method:** POST
  - **Path:** `/feedings`
  - **Status Code:** 201 Created

- **Get Feedings**
  - **Method:** GET
  - **Path:** `/feedings`
  - **Filters:** `animalId`, `date`
  - **Pagination:** `page`, `size`
  - **Status Code:** 200 OK

- **Get Feeding by ID**
  - **Method:** GET
  - **Path:** `/feedings/{id}`
  - **Status Code:** 200 OK, 404 Not Found

- **Update Feeding**
  - **Method:** PUT
  - **Path:** `/feedings/{id}`
  - **Status Code:** 200 OK, 404 Not Found

- **Delete Feeding**
  - **Method:** DELETE
  - **Path:** `/feedings/{id}`
  - **Status Code:** 204 No Content, 404 Not Found

### Alert

- **Create Alert**
  - **Method:** POST
  - **Path:** `/alerts`
  - **Status Code:** 201 Created

- **Get Alerts**
  - **Method:** GET
  - **Path:** `/alerts`
  - **Filters:** `animalId`, `type`
  - **Pagination:** `page`, `size`
  - **Status Code:** 200 OK

- **Get Alert by ID**
  - **Method:** GET
  - **Path:** `/alerts/{id}`
  - **Status Code:** 200 OK, 404 Not Found

- **Update Alert**
  - **Method:** PUT
  - **Path:** `/alerts/{id}`
  - **Status Code:** 200 OK, 404 Not Found

- **Delete Alert**
  - **Method:** DELETE
  - **Path:** `/alerts/{id}`
  - **Status Code:** 204 No Content, 404 Not Found

### Food

- **Create Food**
  - **Method:** POST
  - **Path:** `/foods`
  - **Status Code:** 201 Created

- **Get Foods**
  - **Method:** GET
  - **Path:** `/foods`
  - **Filters:** `type`
  - **Pagination:** `page`, `size`
  - **Status Code:** 200 OK

- **Get Food by ID**
  - **Method:** GET
  - **Path:** `/foods/{id}`
  - **Status Code:** 200 OK, 404 Not Found

- **Update Food**
  - **Method:** PUT
  - **Path:** `/foods/{id}`
  - **Status Code:** 200 OK, 404 Not Found

- **Delete Food**
  - **Method:** DELETE
  - **Path:** `/foods/{id}`
  - **Status Code:** 204 No Content, 404 Not Found

### Nutrition

- **Create Nutrition**
  - **Method:** POST
  - **Path:** `/nutritions`
  - **Status Code:** 201 Created

- **Get Nutritions**
  - **Method:** GET
  - **Path:** `/nutritions`
  - **Filters:** `animalId`
  - **Pagination:** `page`, `size`
  - **Status Code:** 200 OK

- **Get Nutrition by ID**
  - **Method:** GET
  - **Path:** `/nutritions/{id}`
  - **Status Code:** 200 OK, 404 Not Found

- **Update Nutrition**
  - **Method:** PUT
  - **Path:** `/nutritions/{id}`
  - **Status Code:** 200 OK, 404 Not Found

- **Delete Nutrition**
  - **Method:** DELETE
  - **Path:** `/nutritions/{id}`
  - **Status Code:** 204 No Content, 404 Not Found

### FeedingPlan

- **Create FeedingPlan**
  - **Method:** POST
  - **Path:** `/feedingplans`
  - **Status Code:** 201 Created

- **Get FeedingPlans**
  - **Method:** GET
  - **Path:** `/feedingplans`
  - **Filters:** `animalId`
  - **Pagination:** `page`, `size`
  - **Status Code:** 200 OK

- **Get FeedingPlan by ID**
  - **Method:** GET
  - **Path:** `/feedingplans/{id}`
  - **Status Code:** 200 OK, 404 Not Found

- **Update FeedingPlan**
  - **Method:** PUT
  - **Path:** `/feedingplans/{id}`
  - **Status Code:** 200 OK, 404 Not Found

- **Delete FeedingPlan**
  - **Method:** DELETE
  - **Path:** `/feedingplans/{id}`
  - **Status Code:** 204 No Content, 404 Not Found

## Functional and Non-Functional Requirements

### Functional
- Management of animals, feeding, and alerts.
- Management of foods, nutritions, and feeding plans.
- Searching and filtering of records.
- Pagination of results.

### Non-Functional
- Authentication using JWT tokens.
- Error handling with clear messages.
- Caching responses for GET operations.
- Compliance with Richardson Maturity Model level 3 through HATEOAS.

## Authentication

Authentication is performed using JWT tokens, which must be included in the `Authorization` header of each request.

## Error Handling

Errors will be handled with meaningful HTTP status codes and clear messages describing the problem.

## Pagination

Pagination is performed using the `page` and `size` parameters in GET requests.

## Caching

`Cache-Control` will be used for GET operations to improve performance and reduce server load.

## Richardson Maturity Model

The API design adheres to the Richardson Maturity Model (RMM) and achieves Level 3 through the 
implementation of HATEOAS (Hypermedia as the Engine of Application State). This enables clients
 to navigate through the API dynamically, discovering available actions based on the current application state.


zooapirestful/
│
├── src/
│   ├── main/
│   │   ├── java/
│   │   │   ├── com/
│   │   │   │   ├── example/
│   │   │   │   │   ├── zooapirestful/
│   │   │   │   │   │   ├── ZooapirestfulApplication.java
│   │   │   │   │   │   ├── config/
│   │   │   │   │   │   │   └── AppConfig.java
│   │   │   │   │   │   ├── controller/
│   │   │   │   │   │   │   ├── AnimalController.java
│   │   │   │   │   │   │   ├── FeedingPlanController.java
│   │   │   │   │   │   │   ├── FoodController.java
│   │   │   │   │   │   │   ├── NutritionController.java
│   │   │   │   │   │   │   ├── AlertController.java
│   │   │   │   │   │   ├── dao/
│   │   │   │   │   │   │   ├── AnimalDAO.java
│   │   │   │   │   │   │   ├── FeedingPlanDAO.java
│   │   │   │   │   │   │   ├── FoodDAO.java
│   │   │   │   │   │   │   ├── NutritionDAO.java
│   │   │   │   │   │   │   ├── AlertDAO.java
│   │   │   │   │   │   ├── model/
│   │   │   │   │   │   │   ├── Animal.java
│   │   │   │   │   │   │   ├── FeedingPlan.java
│   │   │   │   │   │   │   ├── Food.java
│   │   │   │   │   │   │   ├── Nutrition.java
│   │   │   │   │   │   │   ├── Alert.java
│   │   │   │   │   │   ├── service/
│   │   │   │   │   │   │   ├── AnimalService.java
│   │   │   │   │   │   │   ├── FeedingPlanService.java
│   │   │   │   │   │   │   ├── FoodService.java
│   │   │   │   │   │   │   ├── NutritionService.java
│   │   │   │   │   │   │   ├── AlertService.java
│   │   │   │   │   │   ├── util/
│   │   │   │   │   │   │   ├── DatabaseUtils.java
│   │   │   │   │   │   │   ├── JwtAuthenticationFilter.java
│   │   │   │   │   │   │   ├── JwtAuthorizationFilter.java
│   │   │   │   │   │   │   └── JwtUtil.java
│   │   ├── resources/
│   │   │   ├── application.properties
│   │   │   ├── schema.sql
│   │   │   ├── data.sql
│   │   ├── test/
│   │   │   ├── java/
│   │   │   │   ├── com/
│   │   │   │   │   ├── example/
│   │   │   │   │   │   ├── zooapirestful/
│   │   │   │   │   │   │   ├── ZooapirestfulApplicationTests.java
│
├── .gitignore
├── README.md
├── pom.xml

